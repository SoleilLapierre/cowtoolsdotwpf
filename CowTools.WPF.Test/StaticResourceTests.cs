﻿using CowTools.WPF.Converters;
using NUnit.Framework;

namespace CowTools.WPF.Test
{
    [TestFixture]
    [SetCulture("en-US")]
    public class StaticResourceTests
    {
        [Test]
        public void ExpectedInstances()
        {
            Assert.IsNotNull(StaticResources.DisplayNameConverter);
            Assert.IsAssignableFrom(typeof(DisplayNameConverter), StaticResources.DisplayNameConverter);
        }
    }
}
