﻿using CowTools.WPF.Converters;
using NUnit.Framework;
using System.ComponentModel;

namespace CowTools.WPF.Test.Converters
{
    [TestFixture]
    [SetCulture("en-US")]
    public class DisplayNameConverterTests
    {
        [SetUp]
        public void Setup()
        {
            _converter = new DisplayNameConverter();
        }


        [Test]
        public void ConvertNull()
        {
            Assert.IsNull(_converter.Convert(null, null, null, null));
        }


        [Test]
        public void FindDisplayName()
        {
            Assert.AreEqual("Bar", _converter.Convert(typeof(SubClassWithAttrib), null, null, null));
            Assert.AreEqual("Bar", _converter.Convert(new SubClassWithAttrib(), null, null, null));
        }


        [Test]
        public void FindDisplayNameOnBaseType()
        {
            Assert.AreEqual("Foo", _converter.Convert(typeof(SubClassWithoutAttrib), null, null, null));
            Assert.AreEqual("Foo", _converter.Convert(new SubClassWithoutAttrib(), null, null, null));
        }


        [Test]
        public void FindTypeName()
        {
            Assert.AreEqual("PlainSubClass", _converter.Convert(typeof(PlainSubClass), null, null, null));
            Assert.AreEqual("PlainSubClass", _converter.Convert(new PlainSubClass(), null, null, null));
        }


        public class PlainBaseClass { }

        public class PlainSubClass : PlainBaseClass { }

        [DisplayName("Foo")]
        public class BaseClassWithAttrib { }

        [DisplayName("Bar")]
        public class SubClassWithAttrib : BaseClassWithAttrib { }

        public class SubClassWithoutAttrib : BaseClassWithAttrib { }


        private DisplayNameConverter _converter;
    }
}
