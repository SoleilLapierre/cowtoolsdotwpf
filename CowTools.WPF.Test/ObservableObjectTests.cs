﻿using NUnit.Framework;
using System.ComponentModel;

namespace CowTools.WPF.Test
{
    [TestFixture]
    [SetCulture("en-US")]
    public class ObservableObjectTests
    {
        public class TestClass : ObservableObject
        {
            public int ValueType
            {
                get => _valueType;
                set => Set(ref _valueType, value, nameof(ValueType));
            }

            public object RefType
            {
                get => _refType;
                set => Set(ref _refType, value, nameof(RefType));
            }

            private int _valueType = 0;
            private object _refType = null;
        }


        [SetUp]
        public void Setup()
        {
            _count = 0;
        }


        [Test]
        public void ValueTypeChanged()
        {
            var obj = new TestClass();
            obj.PropertyChanged += OnPropertyChanged;
            Assert.AreEqual(0, _count);
            obj.ValueType = 1;
            Assert.AreEqual(1, _count);
            obj.ValueType = 1;
            Assert.AreEqual(1, _count);
        }


        [Test]
        public void ReferenceTypeChanged()
        {
            var obj = new TestClass();
            obj.PropertyChanged += OnPropertyChanged;
            Assert.AreEqual(0, _count);
            var obj1 = new object();
            var obj2 = new object();

            obj.RefType = obj1;
            Assert.AreEqual(1, _count);
            obj.RefType = obj2;
            Assert.AreEqual(2, _count);
            obj.RefType = obj2;
            Assert.AreEqual(2, _count);
            obj.RefType = null;
            Assert.AreEqual(3, _count);
        }


        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _count++;
        }

        private int _count = 0;
    }
}
