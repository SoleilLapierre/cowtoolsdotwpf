﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace CowTools.WPF.Test
{
    [TestFixture]
    [SetCulture("en-US")]
    public class RelayCommandTests
    {
        [SetUp]
        public void Setup()
        {
            _cmd = null;
            _canExecuteCalls = 0;
            _executeCalls = 0;
            _canExecute = true;
        }


        [Test]
        public void DefaultImplementations()
        {
            Assert.IsTrue(RelayCommand.DoNothing.CanExecute(null));
            Assert.IsFalse(RelayCommand.CanDoNothing.CanExecute(null));
        }


        [Test]
        public void OnDemandInstantiatesOnce()
        {
            Assert.IsNull(_cmd);
            var iCmd = RelayCommand.OnDemand(ref _cmd, Execute, CanExecute);
            Assert.IsNotNull(iCmd);
            Assert.AreEqual(_cmd, iCmd);

            var iCmd2 = RelayCommand.OnDemand(ref _cmd, Execute, CanExecute);
            Assert.IsNotNull(iCmd2);
            Assert.AreEqual(_cmd, iCmd2);

            Assert.AreEqual(0, _canExecuteCalls);
            Assert.AreEqual(0, _executeCalls);
        }


        [Test]
        public void CanExecuteChanged()
        {
            _cmd = new RelayCommand(Execute, CanExecute);

            int changes = 0;
            _cmd.CanExecuteChanged += (_, __) =>
            {
                changes++;
            };

            _cmd.FireCanExecuteChanged();

            Assert.AreEqual(1, changes);
            Assert.AreEqual(0, _canExecuteCalls);
            Assert.AreEqual(0, _executeCalls);
        }


        [Test]
        public void Callbacks()
        {
            _cmd = new RelayCommand(Execute, CanExecute);
            Assert.IsTrue(_cmd.CanExecute(null));
            _canExecute = false;
            Assert.IsFalse(_cmd.CanExecute(null));
            _cmd.Execute(null);
            Assert.AreEqual(2, _canExecuteCalls);
            Assert.AreEqual(1, _executeCalls);
        }


        private bool CanExecute(object _)
        {
            _canExecuteCalls++;
            return _canExecute;
        }


        private void Execute(object _)
        {
            _executeCalls++;
        }


        private RelayCommand _cmd;
        private int _canExecuteCalls;
        private int _executeCalls;
        private bool _canExecute;
    }
}
