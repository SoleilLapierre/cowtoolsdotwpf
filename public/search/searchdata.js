var indexSectionsWithContent =
{
  0: "bcdefgnoprst",
  1: "dors",
  2: "c",
  3: "cefgnors",
  4: "bst",
  5: "cdst",
  6: "p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "properties",
  6: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Properties",
  6: "Events"
};

