This package exists to reduce the frequency with which I rewrite the same basic
functionality in my WPF programs. 

## NO SUPPORT

This is a personal project; no support is provided, content is subject to radical change without 
notice, and it may be abandoned or deleted at any time.

## Functionality

General functionality provided:
* Basic implementation of INotifyPropertyChanged to use as a base class for ViewModels.
* Basic implementation of ICommand, and a helper to instantiate a command only the first time
  it is retrieved.
