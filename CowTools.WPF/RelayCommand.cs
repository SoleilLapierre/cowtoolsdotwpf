﻿using System;
using System.Windows.Input;

namespace CowTools.WPF
{
    /// <summary>
    /// Basic implementation of ICommand that delegates the CanExecute and Execute calls to
    /// user-provided Actions.
    /// </summary>
    /// <remarks>This is based on code I originaly wrote for Zaber Technologies and released
    /// under the Apache 2.0 license, and the original code was in turn based on multiple
    /// examples found online under unstated, CodeProject and Stack Overflow licenses.</remarks>
    public class RelayCommand : ICommand
    {
        #region -- Setup --

        /// <summary>
        /// Initializes a new instance of the RelayCommand class that can always execute.
        /// </summary>
        /// <param name="aExecute">The execute action.</param>
        public RelayCommand(Action<object> aExecute)
        : this(aExecute, _ => true)
        {
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="RelayCommand"/> class.
        /// </summary>
        /// <param name="aExecute">The execute action.</param>
        /// <param name="aCanExecute">The can execute predicate.</param>
        public RelayCommand(Action<object> aExecute, Predicate<object> aCanExecute)
        {
            _executeAction = aExecute ?? throw new ArgumentNullException(nameof(aExecute) + " must not be null.");
            _canExecutePredicate = aCanExecute ?? throw new ArgumentNullException(nameof(aCanExecute) + " must not be null.");
        }

        #endregion
        #region -- Default implementations --

        /// <summary>
        /// An ICommand that can execute but does nothing, to be used as a default.
        /// </summary>
        public static RelayCommand DoNothing { get; } = new RelayCommand(_ => { });


        /// <summary>
        /// An ICommand that does nothing and cannot execute, to be used as a default.
        /// </summary>
        public static RelayCommand CanDoNothing { get; } = new RelayCommand(_ => { }, _ => false);

        #endregion
        #region -- ICommand implementation --

        /// <summary>
        /// Occurs when changes occur that affect whether or not the command should execute.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
                _canExecuteChangedRef += value;
            }
            remove
            {
                _canExecuteChangedRef -= value;
                CommandManager.RequerySuggested -= value;
            }
        }


        /// <summary>
        /// Defines the method that determines whether the command can execute in its current state.
        /// </summary>
        /// <param name="aParam">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        /// <returns>
        /// true if this command can be executed; otherwise, false.
        /// </returns>
        public bool CanExecute(object aParam)
        {
            return _canExecutePredicate(aParam);
        }


        /// <summary>
        /// Defines the method to be called when the command is invoked.
        /// </summary>
        /// <param name="aParam">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
        public void Execute(object aParam)
        {
            _executeAction(aParam);
        }

        #endregion
        #region -- Helpers --

        /// <summary>
        /// Delierately invoke the <see cref="CanExecuteChanged"/> to cause associated controls to re-evaluate their enabled state.
        /// </summary>
        public void FireCanExecuteChanged()
        {
            _canExecuteChangedRef?.Invoke(this, EventArgs.Empty);
        }


        /// <summary>
        /// Helper for creating ICommand handlers only once, on first access. This prevents creating 
        /// multiple copies of the same predicate when the code is the same every time.
        /// </summary>
        /// <param name="aField">Reference to the field to store the command handler in.</param>
        /// <param name="aExecute">Code for the command to execute.</param>
        /// <returns></returns>
        public static ICommand OnDemand(ref RelayCommand aField, Action<object> aExecute)
        {
            if (aField is null)
            {
                aField = new RelayCommand(aExecute);
            }

            return aField;
        }


        /// <summary>
        /// Helper for creating ICommand handlers only once, on first access. This prevents creating 
        /// multiple copies of the same predicate when the code is the same every time.
        /// </summary>
        /// <param name="aField">Reference to the field to store the command handler in.</param>
        /// <param name="aExecute">Code for the command to execute.</param>
        /// <param name="aCanExecute">Code to determine whether the command can be executed.</param>
        /// <returns></returns>
        public static ICommand OnDemand(ref RelayCommand aField, Action<object> aExecute, Predicate<object> aCanExecute)
        {
            if (aField is null)
            {
                aField = new RelayCommand(aExecute, aCanExecute);
            }

            return aField;
        }

        #endregion
        #region -- Data members --

        private readonly Predicate<object> _canExecutePredicate;
        private readonly Action<object> _executeAction;

        // This is just for lifetime management and never actually fires.
        // See https://stackoverflow.com/questions/2281566/is-josh-smiths-implementation-of-the-relaycommand-flawed
        private event EventHandler _canExecuteChangedRef;

        #endregion
    }
}
