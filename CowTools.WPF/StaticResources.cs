﻿using CowTools.WPF.Converters;

namespace CowTools.WPF
{
    /// <summary>
    /// Container for stateless resource class instances that can be
    /// safely shared between many controls, to reduce the need to add
    /// resource dictionaries to your controls. 
    /// To use these, add
    /// <code>xmlns:CowResources="clr-namespace:ZaberWpfToolbox.Resources;assembly=ZaberWpfToolbox"</code>
    /// to your XAML namespaces and reference it in a binding with
    /// <code>Converter={x:Static CowResources:StaticResources.DisplayNameConverter}</code>
    /// </summary>
    public static class StaticResources
    {

        /// <summary>
        /// Display name converter. Returns the display name attribute value of a class or
        /// type, or the class name if there is no attribute, or null if given null.
        /// Does not do reverse conversion.
        /// </summary>
        public static DisplayNameConverter DisplayNameConverter { get; } = new DisplayNameConverter();
    }
}
