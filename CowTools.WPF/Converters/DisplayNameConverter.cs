﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Data;

namespace CowTools.WPF.Converters
{
	/// <summary>
	/// Value Converter that returns the display name of a type or instance. 
	/// If given a type, it will return the value of the DisplayName attribute on that type
	/// or any of its supertypes. If the attribute is not found in the inheritance tree
	/// then it will return the simple name of the type.
	/// If given a class instance, it will to the same on its type.
	/// Returns a null string if the input is null.
	/// </summary>
	/// <remarks>This is mostly copied from a previous implementation, also open-source, that
	/// I implemented for Zaber Technologies.</remarks>
	public class DisplayNameConverter : IValueConverter
	{
		#region -- Public Methods & Properties --

		/// <summary>
		///     Extract the display name from a type or object.
		/// </summary>
		/// <param name="aValue">The type or instance to get the display name of.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParameter">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>The display name of aValue, or its type name if there is no display name.</returns>
		public object Convert(object aValue, Type aTargetType, object aParameter, CultureInfo aCulture)
		{
			if (aValue is null)
			{
				return null;
			}

			if (!(aValue is Type t))
			{
				t = aValue.GetType();
			}

			return GetDisplayName(t) ?? t.Name;
		}


		/// <summary>
		///     Reverse conversion is not implemented.
		/// </summary>
		/// <param name="aValue">Unused.</param>
		/// <param name="aTargetType">Unused.</param>
		/// <param name="aParameter">Unused.</param>
		/// <param name="aCulture">Unused.</param>
		/// <returns>Nothing.</returns>
		/// <exception cref="NotImplementedException">The method was called.</exception>
		public object ConvertBack(object aValue, Type aTargetType, object aParameter, CultureInfo aCulture)
			=> throw new NotImplementedException();

		#endregion

		#region -- Non-Public Methods --

		private static string GetDisplayName(Type aType)
		{
			foreach (var attr in aType.GetCustomAttributes(typeof(DisplayNameAttribute), true))
			{
				var dna = attr as DisplayNameAttribute;
				if (null != dna)
				{
					return dna.DisplayName;
				}
			}

			return null;
		}

		#endregion
	}
}
