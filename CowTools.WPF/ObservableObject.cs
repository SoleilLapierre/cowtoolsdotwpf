﻿using System.ComponentModel;

namespace CowTools.WPF
{
    /// <summary>
    /// Base class for ViewModels. Provides an implementation of INotifyPropertyChanged.
    /// </summary>
    /// <remarks>Disclosure: This is based on code I wrote for Zaber Technologies, which
    /// was originally released under the Apache 2.0 license.</remarks>
    public class ObservableObject : INotifyPropertyChanged
    {
        /// <inheritdoc/>
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// Helper for property setters to automatically fire property changed
        /// events when the value actually changes. Requires use of a backing field.
        /// </summary>
        /// <typeparam name="T">Type of the bound property being changed.</typeparam>
        /// <param name="aField">Reference to the backing field being assigned a new value.</param>
        /// <param name="aValue">New value for the backing field.</param>
        /// <param name="aPropertyName">Name of the bound property being changed.</param>
        /// <returns>True if the event was fired (ie the property did change)</returns>
        public bool Set<T>(ref T aField, T aValue, string aPropertyName)
        {
            if (typeof(T).IsValueType)
            {
                if (!aField.Equals(aValue))
                {
                    aField = aValue;
                    NotifyPropertyChanged(aPropertyName);
                    return true;
                }
            }
            else if (!ReferenceEquals(aField, aValue))
            {
                aField = aValue;
                NotifyPropertyChanged(aPropertyName);
                return true;
            }

            return false;
        }


        /// <summary>
        /// Explicitly fires the property changed eveny. Use when values of 
        /// observed properties are changed outside of a property setter, or 
        /// where <see cref="Set{T}(ref T, T, string)"/> is not appropriate.
        /// </summary>
        /// <param name="aPropertyName">Name of the bound property being changed.</param>
        public void NotifyPropertyChanged(string aPropertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(aPropertyName));
        }
    }
}
