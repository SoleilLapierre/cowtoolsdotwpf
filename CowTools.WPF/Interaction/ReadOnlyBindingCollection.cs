﻿using System.Windows;

namespace CowTools.WPF.Interaction
{
    /// <summary>
    /// Collection for attaching multiple <see cref="ReadOnlyBinding"/> instances
    /// to a XAML control.
    /// </summary>
    public class ReadOnlyBindingCollection : FreezableCollection<ReadOnlyBinding>
    {
    }
}
