﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace CowTools.WPF.Interaction
{
    /// <summary>
    /// Helper for binding read-only dependency properties to ViewModel properties. 
    /// To use, add a ReadonlyBinding.Bindings collection to your XAML control, and
    /// populate it with instances. Set the Source property of each to the read-only
    /// property you want to monitor, and the Target to a OneWayToSource binding to
    /// the property on your ViewModel.
    /// </summary>
    /// <remarks>
    /// This implementation is based on several examples found online.
    /// </remarks>
    public class ReadOnlyBinding : Freezable
    {
        #region -- Dependency properties --

        /// <summary>
        /// Dependency property for the data source (the read-only dependency property to monitor).
        /// </summary>
        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register(
            "Source", 
            typeof(object), 
            typeof(ReadOnlyBinding), 
            new FrameworkPropertyMetadata(null, HandleSourceChanged));


        /// <summary>
        /// Dependency property for the data destination. This should have the OneWayToSource mode.
        /// </summary>
        public static readonly DependencyProperty TargetProperty = DependencyProperty.Register(
            "Target",
            typeof(object),
            typeof(ReadOnlyBinding),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        /// <summary>
        /// Dependency property for the collection of read-only bindings for a given control.
        /// </summary>
        public static readonly DependencyProperty BindingsProperty = DependencyProperty.RegisterAttached(
            "_bindings",
            typeof(ReadOnlyBindingCollection),
            typeof(ReadOnlyBinding),
            new UIPropertyMetadata(null));

        #endregion
        #region -- Dependency property getters and setters --

        /// <summary>
        /// The value of the read-only source dependency property.
        /// </summary>
        public object Source 
        { 
            get => (object)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }


        /// <summary>
        /// The value of the target property.
        /// </summary>
        public object Target
        {
            get => (object)GetValue(TargetProperty);
            set => SetValue(TargetProperty, value);
        }


        /// <summary>
        /// Getter for the collection of read-only bindings on a control.
        /// </summary>
        /// <param name="aControl">The control to which the collection is attached.</param>
        /// <returns>The collection of <see cref="ReadOnlyBinding"/> instances for the control.</returns>
        public static ReadOnlyBindingCollection GetBindings(DependencyObject aControl)
        {
            var pipes = (ReadOnlyBindingCollection)aControl.GetValue(BindingsProperty);
            if (pipes is null)
            {
                aControl.SetValue(BindingsProperty, pipes = new ReadOnlyBindingCollection());
            }

            return pipes;
        }


        /// <summary>
        /// Setter for the collection of read-only bindings on a control.
        /// </summary>
        /// <param name="aControl">The control to set the collection on.</param>
        /// <param name="aValue">The new collection to attach to the control.</param>
        public static void SetBindings(DependencyObject aControl, ReadOnlyBindingCollection aValue)
        {
            aControl.SetValue(BindingsProperty, aValue);
        }

        #endregion
        #region -- Internal functionality --

        // Passes source property value changes to the target.
        static void HandleSourceChanged(DependencyObject aSourceObject, DependencyPropertyChangedEventArgs aArgs) 
        { 
            if (aSourceObject is ReadOnlyBinding dest)
            {
                if (dest.Target != aArgs.NewValue)
                {
                    dest.Target = aArgs.NewValue;
                }
            }
        }

        #endregion
        #region -- Base class overrides --

        /// <summary>
        /// Implement abstract base class method.
        /// </summary>
        /// <returns>A new instance of <see cref="ReadOnlyBinding"/>.</returns>
        protected override Freezable CreateInstanceCore() 
        {
            return new ReadOnlyBinding(); 
        }

        #endregion
    }
}
